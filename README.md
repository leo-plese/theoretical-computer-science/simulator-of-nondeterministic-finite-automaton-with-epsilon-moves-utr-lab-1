Simulator of nondeterministic finite automaton with epsilon moves. Implemented in Java.

My lab assignment in Introduction to Theoretical Computer Science, FER, Zagreb.

Created: 2019
