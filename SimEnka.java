import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;


public class SimEnka {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		 
		String input = reader.readLine();
		String states = reader.readLine();
		String alphabet = reader.readLine();
		String acceptableStates = reader.readLine();
		String initialState = reader.readLine();


		List<String> transitions = new ArrayList<>();

		String t = reader.readLine();
		while (t != null && !t.trim().isEmpty()) {
			transitions.add(t);
			t = reader.readLine();
		}

		

		String[] inputsI = input.split("\\|");
		String[] statesQ = states.split(",");
		String[] alphabetS = alphabet.split(",");
		String[] alphabetWithEps = new String[alphabetS.length + 1];
		for (int i = 0; i < alphabetS.length; i++) {
			alphabetWithEps[i] = alphabetS[i];
		}
		alphabetWithEps[alphabetWithEps.length - 1] = "$";
		alphabetS = alphabetWithEps;
		String[] acceptableStatesQ = acceptableStates.split(",");
		String[] initialStateQ = initialState.split(",");
		Set<String> initialStates = new LinkedHashSet<>(Arrays.asList(initialStateQ));

		if (acceptableStatesQ.length == 1 && acceptableStatesQ[0].isEmpty())
			acceptableStatesQ = new String[0];

		Map<StateCharPair, List<String>> map = makeMap(transitions);

		map = addRemainingToMap(map, statesQ, alphabetS);

		Map<String, Set<String>> mapStateEpsilon = new LinkedHashMap<>();
		for (String s : statesQ) {
			mapStateEpsilon.put(s, getEpsilonEnvironment(s, map, new TreeSet<>()));
		}

		StringBuilder statesPrint = new StringBuilder("");

		Set<String> state = new TreeSet<>();
		Set<String> newStatesSet = new TreeSet<>();
		for (String inp : inputsI) {

			for (String s : initialStates) {
				state.addAll(getEpsilonEnvironment(s, map, new TreeSet<>()));
			}

			String statesPrintPart = "";
			for (String s : state) {
				if (s.equals("#"))
					continue;
				statesPrintPart += s + ",";
			}
			statesPrintPart = statesPrintPart.substring(0, statesPrintPart.length() - 1) + "|";

			statesPrint.append(statesPrintPart);

			state = new TreeSet<>();
			state.addAll(initialStates);
			String[] inChs = inp.split(",");

			boolean firstChar = true;
			for (String ch : inChs) {

				if (state.size() == 1 && state.contains("#")) {
					statesPrint.append("#|");
					continue;
				}
				if (firstChar) {
					firstChar = false;
				} else {
					state.clear();
					state.addAll(newStatesSet);
				}

				newStatesSet.clear();

				Iterator<String> it = state.iterator();
				while (it.hasNext()) {
					String s = it.next();
					if (s.equals("#")) {
						it.remove();
					}
				}

				Set<String> epsEnvStates = null;
				if (state.size() != 0) {
					epsEnvStates = new TreeSet<>();
					for (String s : state) {
						if (s.equals("#"))
							continue;/////////
						epsEnvStates.addAll(mapStateEpsilon.get(s));
					}
				} else {
					statesPrint.append("#|");
					state.clear();
					state.add("#");
					continue;
				}

				Set<String> statesFromEpsEnvStates = new LinkedHashSet<>();
				if (epsEnvStates != null) {
					for (String s : epsEnvStates) {
						List<String> lst = map.get(new StateCharPair(s, ch));
						statesFromEpsEnvStates.addAll(lst);
					}
				}

				for (String s : statesFromEpsEnvStates) {
					newStatesSet.addAll(getEpsilonEnvironment(s, map, new TreeSet<>()));
				}

				if (newStatesSet.contains("#")) {
					if (newStatesSet.size() == 1) {
						newStatesSet.clear();
						statesPrint.append("#|");
						state.clear();
						state.add("#");
						continue;
					} else if (newStatesSet.size() >= 2) {
						Iterator<String> iter = newStatesSet.iterator();
						while (iter.hasNext()) {
							String s = iter.next();
							if (s.equals("#")) {
								iter.remove();
							}
						}
					}
				}
				String printPart = "";
				for (String s : newStatesSet) {
					printPart += s + ",";
				}
				printPart = printPart.substring(0, printPart.length() - 1) + "|";
				statesPrint.append(printPart);

				state.clear();
			}
			statesPrint = new StringBuilder(statesPrint.substring(0, statesPrint.length() - 1));
			statesPrint.append("\n");
		}
		System.out.println(statesPrint.substring(0).trim());

		reader.close();
	}

	private static Set<String> getEpsilonEnvironment(String state, Map<StateCharPair, List<String>> map,
			Set<String> epsStates) {
		String currentState = state;
		int currentInd = 0;
		List<String> states = new ArrayList<>();
		states.add(currentState);

		while (currentInd < states.size()) {
			currentState = states.get(currentInd);
			List<String> newStates = map.get(new StateCharPair(currentState, "$"));

			if (currentState.equals("#") || newStates.contains("#")) {
				currentInd++;
				continue;
			}
			for (String s : newStates) {
				if (!states.contains(s)) {
					states.add(s);
				}
			}

			currentInd++;

		}

		epsStates.addAll(states);

		return epsStates;

	}

	private static Map<StateCharPair, List<String>> addRemainingToMap(Map<StateCharPair, List<String>> map,
			String[] statesQ, String[] alphabetS) {
		Map<StateCharPair, List<String>> mapOld = new LinkedHashMap<>();
		map.forEach((k, v) -> mapOld.put(k, v));

		map.forEach((k, v) -> {
			for (String state : statesQ) {
				for (String inChar : alphabetS) {
					StateCharPair pair = new StateCharPair(state, inChar);
					if (!mapOld.containsKey(pair)) {
						mapOld.put(pair, new ArrayList<String>(Arrays.asList("#")));
					}
				}
			}
		});

		return mapOld;
	}

	private static Map<StateCharPair, List<String>> makeMap(List<String> transitions) {
		Map<StateCharPair, List<String>> map = new LinkedHashMap<>();
		for (String t : transitions) {
			String[] lr = t.split("->");
			String[] l = lr[0].split(",");
			StateCharPair pair = new StateCharPair(l[0], l[1]);
			String[] newStates = lr[1].split(",");

			List<String> newQs = new ArrayList<>(Arrays.asList(newStates));

			map.put(pair, newQs);
		}

		return map;

	}

	private static class StateCharPair {
		private String state;
		private String inChar;

		public StateCharPair(String state, String inChar) {
			super();
			this.state = state;
			this.inChar = inChar;
		}

		@Override
		public int hashCode() {
			return Objects.hash(inChar, state);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof StateCharPair))
				return false;
			StateCharPair other = (StateCharPair) obj;
			return Objects.equals(inChar, other.inChar) && Objects.equals(state, other.state);
		}

	}

}
